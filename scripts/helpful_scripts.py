from brownie import accounts, network, config


def getAccount(index=None, id=None):
    # accounts[0]
    # accounts.add("env")
    # accounts.load("id")
    if index:
        return accounts[index]
    if id:
        return accounts.load(id)
    if network.show_active() == "development":
        return accounts[0]
    return accounts.add(config["wallets"]["from_key"])
