from brownie import Rimuru, config, network
from click import prompt
from scripts.helpful_scripts import getAccount


def deploy():
    supply = prompt("Enter initial suply: ")
    account = getAccount()
    rimuru = Rimuru.deploy(
        supply,
        {"from": account},
        publish_source=config["networks"][network.show_active()].get("verify", False),
    )
    print("Deployed Rimuru ERC20 Token!")
    print(account.balance())
    return rimuru


def main():
    deploy()
