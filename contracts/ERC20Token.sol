// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Rimuru is ERC20 {
    uint256 public total_minted;

    constructor(uint256 _initialSupply) public ERC20("Rimuru Coins", "SLM") {
        uint256 initialSupply = _initialSupply * 10**18;
        _mint(msg.sender, initialSupply);
        total_minted = initialSupply;
    }

    function mintMore(uint256 _supply) public {
        uint256 supply = _supply * 10**18;
        _mint(msg.sender, supply);
        total_minted += supply;
    }
}
