from brownie import Rimuru
from click import prompt
from scripts.helpful_scripts import getAccount


def mintMore(amount_to_mint):
    account = getAccount()
    rimuru = Rimuru[-1]
    rimuru.mintMore(amount_to_mint, {"from": account})
    print(rimuru.total_minted())


def main():
    amount_to_mint = prompt("Enter amount to be minted: ")
    mintMore(amount_to_mint)
